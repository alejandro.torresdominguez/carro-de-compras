class carro{
    constructor(art1,art2,art3,art4){
        this.art1 = art1;
        this.art2 = art2;
        this.art3 = art3;
        this.art4 = art4;
    }
    get calculo(){
        return this.total();
    }
    total(){
        return this.art1 + this.art2 + this.art3 + this.art4;
    }
}

const resultado = new carro(100,200,145,300);

console.log(resultado.calculo);